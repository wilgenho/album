package com.ma.album.aplicacion;

import com.ma.album.dominio.Album;
import com.ma.album.dominio.AlbumRepository;
import org.springframework.stereotype.Service;

@Service
public class ConsultarAlbum {

    private final AlbumRepository albumRepository;

    public ConsultarAlbum(final AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    public Album consultar(int id) {
        if (id < 0) {
            throw new IllegalArgumentException("Numero de id incorrecto");
        }
        return albumRepository.consultar(id);
    }
}
