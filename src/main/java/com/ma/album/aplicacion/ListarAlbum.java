package com.ma.album.aplicacion;

import com.ma.album.dominio.Album;
import com.ma.album.dominio.AlbumRepository;
import com.ma.shared.dominio.Lista;
import org.springframework.stereotype.Service;

@Service
public class ListarAlbum {

    private final AlbumRepository albumRepository;

    public ListarAlbum(final AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    public Lista<Album> listar(int pagina, int limite) {
        if (pagina < 0) {
            throw new IllegalArgumentException("Numero de pagina incorrecto");
        }
        if (limite < 1 || limite > 20) {
            throw new IllegalArgumentException("Limite incorrecto");
        }
        return albumRepository.listar(pagina, limite);
    }
}
