package com.ma.album.dominio;

import lombok.Value;

@Value
public class Album {

    int id;
    int usuario;
    String titulo;

}
