package com.ma.album.dominio;

import com.ma.shared.dominio.Lista;

public interface AlbumRepository {
    Lista<Album> listar(int pagina, int limite);
    Album consultar(int id);
}
