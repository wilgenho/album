package com.ma.album.infra;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "album", url="${app.base-url}")
public interface AlbumClient {

    @GetMapping("albums/{id}")
    public AlbumDto consultar(@PathVariable int id);

    @GetMapping("albums")
    public ResponseEntity<List<AlbumDto>> listar(
            @RequestParam("_page") int page,
            @RequestParam("_limit") int limit
    );

}
