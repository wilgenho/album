package com.ma.album.infra;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ma.album.dominio.Album;
import lombok.Data;

@Data
public class AlbumDto {

    @JsonProperty("userId")
    private int usuario;

    private int id;

    @JsonProperty("title")
    private String titulo;

    public Album toDomain() {
        return new Album(id, usuario, titulo);
    }

}
