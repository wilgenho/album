package com.ma.album.infra;

import com.ma.album.dominio.Album;
import com.ma.album.dominio.AlbumRepository;
import com.ma.shared.dominio.Lista;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AlbumRepositoryImpl implements AlbumRepository {

    private final AlbumClient albumClient;

    public AlbumRepositoryImpl(final AlbumClient albumClient) {
        this.albumClient = albumClient;
    }

    @Override
    public Lista<Album> listar(int pagina, int limite) {
        ResponseEntity<List<AlbumDto>> responseEntity = albumClient.listar(pagina, limite);
        return new Lista<>(getTotal(responseEntity), getLista(responseEntity));
    }

    @Override
    public Album consultar(int id) {
        return albumClient.consultar(id).toDomain();
    }

    private int getTotal(ResponseEntity<List<AlbumDto>> responseEntity) {
        String total =  responseEntity.getHeaders().getFirst("x-total-count");
        return total == null ? 0 : Integer.parseInt(total);
    }
    private List<Album> getLista(ResponseEntity<List<AlbumDto>> responseEntity) {
        return responseEntity.getBody() == null? Collections.emptyList():
                responseEntity.getBody().stream().map(AlbumDto::toDomain).collect(Collectors.toList());
    }

}
