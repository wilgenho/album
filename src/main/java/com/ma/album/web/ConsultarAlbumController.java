package com.ma.album.web;

import com.ma.album.aplicacion.ConsultarAlbum;
import com.ma.album.dominio.Album;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsultarAlbumController {

    private final ConsultarAlbum consultarAlbum;

    public ConsultarAlbumController(final ConsultarAlbum consultarAlbum) {
        this.consultarAlbum = consultarAlbum;
    }

    @GetMapping("albums/{id}")
    public Album consultar(@PathVariable int id) {
        return consultarAlbum.consultar(id);
    }

}
