package com.ma.album.web;

import com.ma.album.aplicacion.ListarAlbum;
import com.ma.album.dominio.Album;
import com.ma.shared.dominio.Lista;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ListarAlbumController {

    private final ListarAlbum listarAlbum;

    public ListarAlbumController(final ListarAlbum listarAlbum) {
        this.listarAlbum = listarAlbum;
    }

    @GetMapping("albums")
    public Lista<Album> listar(
            @RequestParam(name = "pagina", required = false, defaultValue = "0") int pagina,
            @RequestParam(name = "limite", required = false, defaultValue = "10") int limite) {
        return listarAlbum.listar(pagina, limite);
    }
}
