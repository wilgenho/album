package com.ma.shared.infra;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.log4j.Log4j2;


@Log4j2
public class ClientInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("x-custom-header", "prueba");
        log.info("Estoy en ClientInterceptor");
    }
}
