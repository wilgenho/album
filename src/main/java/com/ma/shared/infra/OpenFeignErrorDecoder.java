package com.ma.shared.infra;

import com.ma.shared.dominio.EntidadNoEncontrada;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class OpenFeignErrorDecoder implements ErrorDecoder {

    private final ErrorDecoder defaultErrorDecoder = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {
        log.warn("Error HTTP {}, codigo {}", methodKey, response.status());
        if ( response.status() == 404) {
            return new EntidadNoEncontrada();
        }
        return defaultErrorDecoder.decode(methodKey, response);
    }
}
