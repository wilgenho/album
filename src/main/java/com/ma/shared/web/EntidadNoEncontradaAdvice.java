package com.ma.shared.web;

import com.ma.shared.dominio.EntidadNoEncontrada;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class EntidadNoEncontradaAdvice {

    @ResponseBody
    @ExceptionHandler(EntidadNoEncontrada.class)
    public ResponseEntity<Void> handle(EntidadNoEncontrada exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
