package com.ma.album.web;

import com.intuit.karate.junit5.Karate;
import com.ma.shared.TestAceptacion;

import static com.github.tomakehurst.wiremock.client.WireMock.*;


class ConsultarAlbumControllerTest extends TestAceptacion {

    @Karate.Test
    Karate deberiaConsultarUnAlbum() {
        stub();
        return Karate.run("classpath:karate/consultar-album.feature");
    }

    private void stub() {
        server.stubFor(
                get(urlEqualTo("/albums/1"))
                .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBodyFile("json/album1.json")));
    }

}