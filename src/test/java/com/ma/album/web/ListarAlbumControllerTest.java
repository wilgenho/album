package com.ma.album.web;

import com.intuit.karate.junit5.Karate;
import com.ma.shared.TestAceptacion;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

class ListarAlbumControllerTest extends TestAceptacion {

    @Karate.Test
    Karate deberiaListarAlbums() {
        stub();
        return Karate.run("classpath:karate/listar-album.feature");
    }

    private void stub() {
        server.stubFor(
                get(urlEqualTo("/albums?_page=0&_limit=2"))
                .willReturn(aResponse()
                .withStatus(200)
                .withHeader("x-total-count", "100")
                .withHeader("Content-Type", "application/json")
                .withBodyFile("json/lista.json")));
    }


}