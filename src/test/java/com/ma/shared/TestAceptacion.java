package com.ma.shared;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TestAceptacion {

    protected static WireMockServer server;

    @BeforeAll
    static void config() {
        server = new WireMockServer(8180);
        server.start();
    }

    @AfterAll
    static void stop() {
        server.stop();
    }

}
