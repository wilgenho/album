#noinspection CucumberUndefinedStep
Feature: Consultar un album de fotos

  Background:
    * url urlBase

  Scenario: Consultar un album existente
    Given path 'albums' , 1
    When method get
    Then status 200
    And match response == {id:1 , usuario: 1 , titulo : 'quidem molestiae enim' }

