#noinspection CucumberUndefinedStep
Feature: Listar albumnes de fotos

  Background:
    * url urlBase

  Scenario: Consultar una lista de albums
    Given path 'albums'
    And param limite = 2
    When method get
    Then status 200
    And match response ==
    """
    {
      total: 100,
      datos: [{
              id: 1,
              usuario: 1,
              titulo: 'quidem molestiae enim'
          }, {
              id: 2,
              usuario: 1,
              titulo: 'sunt qui excepturi placeat culpa'
          }]
    }
    """

